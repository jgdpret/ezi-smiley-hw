/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC16F15354
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.45
        MPLAB 	          :  MPLAB X 4.15	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set IO_B1 aliases
#define IO_B1_TRIS                 TRISAbits.TRISA0
#define IO_B1_LAT                  LATAbits.LATA0
#define IO_B1_PORT                 PORTAbits.RA0
#define IO_B1_WPU                  WPUAbits.WPUA0
#define IO_B1_OD                   ODCONAbits.ODCA0
#define IO_B1_ANS                  ANSELAbits.ANSA0
#define IO_B1_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define IO_B1_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define IO_B1_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define IO_B1_GetValue()           PORTAbits.RA0
#define IO_B1_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define IO_B1_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define IO_B1_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define IO_B1_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define IO_B1_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define IO_B1_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define IO_B1_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define IO_B1_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set IO_B2 aliases
#define IO_B2_TRIS                 TRISAbits.TRISA1
#define IO_B2_LAT                  LATAbits.LATA1
#define IO_B2_PORT                 PORTAbits.RA1
#define IO_B2_WPU                  WPUAbits.WPUA1
#define IO_B2_OD                   ODCONAbits.ODCA1
#define IO_B2_ANS                  ANSELAbits.ANSA1
#define IO_B2_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define IO_B2_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define IO_B2_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define IO_B2_GetValue()           PORTAbits.RA1
#define IO_B2_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define IO_B2_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define IO_B2_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define IO_B2_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define IO_B2_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define IO_B2_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define IO_B2_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define IO_B2_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set IO_B3 aliases
#define IO_B3_TRIS                 TRISAbits.TRISA2
#define IO_B3_LAT                  LATAbits.LATA2
#define IO_B3_PORT                 PORTAbits.RA2
#define IO_B3_WPU                  WPUAbits.WPUA2
#define IO_B3_OD                   ODCONAbits.ODCA2
#define IO_B3_ANS                  ANSELAbits.ANSA2
#define IO_B3_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define IO_B3_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define IO_B3_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define IO_B3_GetValue()           PORTAbits.RA2
#define IO_B3_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define IO_B3_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define IO_B3_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define IO_B3_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define IO_B3_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define IO_B3_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define IO_B3_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define IO_B3_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set IO_B4 aliases
#define IO_B4_TRIS                 TRISAbits.TRISA3
#define IO_B4_LAT                  LATAbits.LATA3
#define IO_B4_PORT                 PORTAbits.RA3
#define IO_B4_WPU                  WPUAbits.WPUA3
#define IO_B4_OD                   ODCONAbits.ODCA3
#define IO_B4_ANS                  ANSELAbits.ANSA3
#define IO_B4_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define IO_B4_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define IO_B4_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define IO_B4_GetValue()           PORTAbits.RA3
#define IO_B4_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define IO_B4_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define IO_B4_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define IO_B4_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define IO_B4_SetPushPull()        do { ODCONAbits.ODCA3 = 0; } while(0)
#define IO_B4_SetOpenDrain()       do { ODCONAbits.ODCA3 = 1; } while(0)
#define IO_B4_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define IO_B4_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set IO_RA4 aliases
#define IO_RA4_TRIS                 TRISAbits.TRISA4
#define IO_RA4_LAT                  LATAbits.LATA4
#define IO_RA4_PORT                 PORTAbits.RA4
#define IO_RA4_WPU                  WPUAbits.WPUA4
#define IO_RA4_OD                   ODCONAbits.ODCA4
#define IO_RA4_ANS                  ANSELAbits.ANSA4
#define IO_RA4_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define IO_RA4_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define IO_RA4_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define IO_RA4_GetValue()           PORTAbits.RA4
#define IO_RA4_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define IO_RA4_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define IO_RA4_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define IO_RA4_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define IO_RA4_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define IO_RA4_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define IO_RA4_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define IO_RA4_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set IO_BUZZER aliases
#define IO_BUZZER_TRIS                 TRISAbits.TRISA5
#define IO_BUZZER_LAT                  LATAbits.LATA5
#define IO_BUZZER_PORT                 PORTAbits.RA5
#define IO_BUZZER_WPU                  WPUAbits.WPUA5
#define IO_BUZZER_OD                   ODCONAbits.ODCA5
#define IO_BUZZER_ANS                  ANSELAbits.ANSA5
#define IO_BUZZER_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define IO_BUZZER_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define IO_BUZZER_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define IO_BUZZER_GetValue()           PORTAbits.RA5
#define IO_BUZZER_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define IO_BUZZER_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define IO_BUZZER_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define IO_BUZZER_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define IO_BUZZER_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define IO_BUZZER_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define IO_BUZZER_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define IO_BUZZER_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set IO_LED1 aliases
#define IO_LED1_TRIS                 TRISAbits.TRISA6
#define IO_LED1_LAT                  LATAbits.LATA6
#define IO_LED1_PORT                 PORTAbits.RA6
#define IO_LED1_WPU                  WPUAbits.WPUA6
#define IO_LED1_OD                   ODCONAbits.ODCA6
#define IO_LED1_ANS                  ANSELAbits.ANSA6
#define IO_LED1_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define IO_LED1_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define IO_LED1_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define IO_LED1_GetValue()           PORTAbits.RA6
#define IO_LED1_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define IO_LED1_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define IO_LED1_SetPullup()          do { WPUAbits.WPUA6 = 1; } while(0)
#define IO_LED1_ResetPullup()        do { WPUAbits.WPUA6 = 0; } while(0)
#define IO_LED1_SetPushPull()        do { ODCONAbits.ODCA6 = 0; } while(0)
#define IO_LED1_SetOpenDrain()       do { ODCONAbits.ODCA6 = 1; } while(0)
#define IO_LED1_SetAnalogMode()      do { ANSELAbits.ANSA6 = 1; } while(0)
#define IO_LED1_SetDigitalMode()     do { ANSELAbits.ANSA6 = 0; } while(0)

// get/set IO_SLEEP aliases
#define IO_SLEEP_TRIS                 TRISBbits.TRISB0
#define IO_SLEEP_LAT                  LATBbits.LATB0
#define IO_SLEEP_PORT                 PORTBbits.RB0
#define IO_SLEEP_WPU                  WPUBbits.WPUB0
#define IO_SLEEP_OD                   ODCONBbits.ODCB0
#define IO_SLEEP_ANS                  ANSELBbits.ANSB0
#define IO_SLEEP_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define IO_SLEEP_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define IO_SLEEP_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define IO_SLEEP_GetValue()           PORTBbits.RB0
#define IO_SLEEP_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define IO_SLEEP_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define IO_SLEEP_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define IO_SLEEP_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define IO_SLEEP_SetPushPull()        do { ODCONBbits.ODCB0 = 0; } while(0)
#define IO_SLEEP_SetOpenDrain()       do { ODCONBbits.ODCB0 = 1; } while(0)
#define IO_SLEEP_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define IO_SLEEP_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set IO_LED2 aliases
#define IO_LED2_TRIS                 TRISCbits.TRISC1
#define IO_LED2_LAT                  LATCbits.LATC1
#define IO_LED2_PORT                 PORTCbits.RC1
#define IO_LED2_WPU                  WPUCbits.WPUC1
#define IO_LED2_OD                   ODCONCbits.ODCC1
#define IO_LED2_ANS                  ANSELCbits.ANSC1
#define IO_LED2_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define IO_LED2_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define IO_LED2_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define IO_LED2_GetValue()           PORTCbits.RC1
#define IO_LED2_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define IO_LED2_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define IO_LED2_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define IO_LED2_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define IO_LED2_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define IO_LED2_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define IO_LED2_SetAnalogMode()      do { ANSELCbits.ANSC1 = 1; } while(0)
#define IO_LED2_SetDigitalMode()     do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set IO_LED3 aliases
#define IO_LED3_TRIS                 TRISCbits.TRISC2
#define IO_LED3_LAT                  LATCbits.LATC2
#define IO_LED3_PORT                 PORTCbits.RC2
#define IO_LED3_WPU                  WPUCbits.WPUC2
#define IO_LED3_OD                   ODCONCbits.ODCC2
#define IO_LED3_ANS                  ANSELCbits.ANSC2
#define IO_LED3_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define IO_LED3_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define IO_LED3_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define IO_LED3_GetValue()           PORTCbits.RC2
#define IO_LED3_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define IO_LED3_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define IO_LED3_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define IO_LED3_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define IO_LED3_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define IO_LED3_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define IO_LED3_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define IO_LED3_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set IO_LED4 aliases
#define IO_LED4_TRIS                 TRISCbits.TRISC3
#define IO_LED4_LAT                  LATCbits.LATC3
#define IO_LED4_PORT                 PORTCbits.RC3
#define IO_LED4_WPU                  WPUCbits.WPUC3
#define IO_LED4_OD                   ODCONCbits.ODCC3
#define IO_LED4_ANS                  ANSELCbits.ANSC3
#define IO_LED4_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define IO_LED4_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define IO_LED4_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define IO_LED4_GetValue()           PORTCbits.RC3
#define IO_LED4_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define IO_LED4_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define IO_LED4_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define IO_LED4_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define IO_LED4_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define IO_LED4_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define IO_LED4_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define IO_LED4_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set IO_RW aliases
#define IO_RW_TRIS                 TRISCbits.TRISC5
#define IO_RW_LAT                  LATCbits.LATC5
#define IO_RW_PORT                 PORTCbits.RC5
#define IO_RW_WPU                  WPUCbits.WPUC5
#define IO_RW_OD                   ODCONCbits.ODCC5
#define IO_RW_ANS                  ANSELCbits.ANSC5
#define IO_RW_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define IO_RW_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define IO_RW_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define IO_RW_GetValue()           PORTCbits.RC5
#define IO_RW_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define IO_RW_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define IO_RW_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define IO_RW_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define IO_RW_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define IO_RW_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define IO_RW_SetAnalogMode()      do { ANSELCbits.ANSC5 = 1; } while(0)
#define IO_RW_SetDigitalMode()     do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetPullup()             do { WPUCbits.WPUC6 = 1; } while(0)
#define RC6_ResetPullup()           do { WPUCbits.WPUC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSC6 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/