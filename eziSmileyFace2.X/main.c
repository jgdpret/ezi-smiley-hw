/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC16F15354
        Driver Version    :  2.00
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
 */

#include "mcc_generated_files/mcc.h"
#include <stdio.h>
#include "ezismileyface2.h"

void ISR_50ms() {
    timerTick = 1;
    
}
/*
                         Main application
 */
void main(void) {
    // initialize the device
    SYSTEM_Initialize();
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    
    IO_RW_SetHigh();
    printf("Initialization Complete\n\r");
    IO_RW_SetHigh();
    
    LATAbits.LATA4 = 0; //Set for highest sensitivity
    
    TMR1_SetInterruptHandler(ISR_50ms); //Define interrupt Handler
    TMR1_StartTimer();
    while (1) {
        //continue;
        if (!button1Pressed && !IO_B1_GetValue()) {
            button1Pressed = 1;     // set button pressed flag for debouncing
            counter1_50ms = 0;      // make sure counter starts at 0
            IO_LED1_SetLow();       // turn on LED 1
            IO_BUZZER_Toggle();     // click the buzzer
            //puts("B1:1");         // send data to Pigeon
            LATBbits.LATB0 = 1; 
            //IO_SLEEP_SetHigh();     // send signal to wake Pigeon
        }
        if (!button2Pressed && !IO_B2_GetValue()) {
            button2Pressed = 1;     // set button pressed flag for debouncing
            counter2_50ms = 0;      // make sure counter starts at 0
            IO_LED2_SetLow();       // turn on LED 2
            IO_BUZZER_Toggle();     // click the buzzer
            //puts("B2:1");         // send data to Pigeon
            LATBbits.LATB0 = 1; 
            //IO_SLEEP_SetHigh();     // send signal to wake Pigeon
        }
        if (!button3Pressed && !IO_B3_GetValue()) {
            button3Pressed = 1;     // set button pressed flag for debouncing
            counter3_50ms = 0;      // make sure counter starts at 0
            IO_LED3_SetLow();       // turn on LED 3
            IO_BUZZER_Toggle();     // click the buzzer
            //puts("B3:1");         // send data to Pigeon
            LATBbits.LATB0 = 1; 
            //IO_SLEEP_SetHigh();     // send signal to wake Pigeon
        }
        if (!button4Pressed && !IO_B4_GetValue()) {
            button4Pressed = 1;     // set button pressed flag for debouncing
            counter4_50ms = 0;      // make sure counter starts at 0
            IO_LED4_SetLow();       // turn on LED 4
            IO_BUZZER_Toggle();     // click the buzzer
            //puts("B4:1");         // send data to Pigeon
            LATBbits.LATB0 = 1; 
            //IO_SLEEP_SetHigh();     // send signal to wake Pigeon
        }
        
        if (timerTick){
            timerTick = 0;              // clear timer flag
            
      //      printf("%d %d %d %d\r\n",IO_B1_GetValue(),IO_B2_GetValue(),IO_B3_GetValue(),IO_B4_GetValue());
            if (button1Pressed && counter1_50ms++ >= 40){       // *** add check for button still pressed
                IO_LED1_SetHigh();      // turn off LED 1
                button1Pressed = 0;     // clear button pressed flag
                printf("B1:1\r");           // send data to Pigeon
                IO_SLEEP_SetLow();      // clear signal to wake Pigeon
            }
            if (button2Pressed && counter2_50ms++ >= 40){       // *** add check for button still pressed
                IO_LED2_SetHigh();      // turn off LED 2
                button2Pressed = 0;     // clear button pressed flag
                printf("B2:1\r");           // send data to Pigeon
                IO_SLEEP_SetLow();      // clear signal to wake Pigeon
            }
            if (button3Pressed && counter3_50ms++ >= 40){       // *** add check for button still pressed
                IO_LED3_SetHigh();      // turn off LED 3
                button3Pressed = 0;     // clear button pressed flag
                printf("B3:1\r");           // send data to Pigeon
                IO_SLEEP_SetLow();      // clear signal to wake Pigeon
            }
            if (button4Pressed && counter4_50ms++ >= 40){       // *** add check for button still pressed
                IO_LED4_SetHigh();      // turn off LED 4
                button4Pressed = 0;     // clear button pressed flag
                printf("B4:1\r");           // send data to Pigeon
                IO_SLEEP_SetLow();      // clear signal to wake Pigeon
            }
        }
    }
}


/**
 End of File
 */